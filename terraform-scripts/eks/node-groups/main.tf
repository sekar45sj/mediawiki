/*
  Adding tags to the subnets dynamically for the EKS cluster Workernodes 
*/
resource "null_resource" "tag-subnets" {
  count = length(var.SUBNETS_ID)
  provisioner "local-exec" {
    command = "aws ec2 create-tags --region ${var.REGION} --resources ${element(var.SUBNETS_ID, count.index)} --tags Key=kubernetes.io/cluster/'${var.CLUSTER_NAME}',Value=shared"
  }
}

/*
  Definition of the AWS Managed worker nodes
*/
resource "aws_eks_node_group" "node" {
  depends_on      = [null_resource.tag-subnets]
  cluster_name    = var.CLUSTER_NAME
  node_group_name = var.NODE_GROUP_NAME
  node_role_arn   = var.NODE_ARN
  subnet_ids      = var.SUBNETS_ID
  ami_type        = var.AMI_TYPE
  disk_size       = var.DISK_SIZE
  instance_types  = [ var.INSTANCE_TYPE ]
  labels          = var.LABELS
  remote_access {
    ec2_ssh_key = var.KEY_PAIR_NAME
  }
  scaling_config {
    desired_size = var.DESIRED_NODES
    max_size     = var.MAX_NODES
    min_size     = var.MIN_NODES
  }
  tags = {
    Name = var.NODE_GROUP_NAME
  }
}
