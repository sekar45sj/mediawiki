variable "REGION" {
  description = "AWS Region"
}

variable "AMI_TYPE" {
  description = "AMI type of the worker nodes"
}

variable "CLUSTER_NAME" {
  description = "Name of the cluster to  join the worker node groups"
}

variable "NODE_GROUP_NAME" {
  description = "Name of the node group to be created"
}

variable "NODE_ARN" {
  description = "IAM role for the worker nodes"
}

variable "SUBNETS_ID" {
  description = "Subnets where the worker nodes have to be created"
}

variable "DISK_SIZE" {
  description = "Size of the instance. Default AWS will assign 20 GB"
}

variable "INSTANCE_TYPE" {
  description = "Instance type of the EC2 instance to be created for the worker nodes"
}

variable "LABELS" {
  description = "Labels for the Node Group"
}

variable "DESIRED_NODES" {
  description = "Desired number of nodes to run"
}

variable "MAX_NODES" {
  description = "Maximum number of nodes in the ASG"
}

variable "MIN_NODES" {
  description = "Minimum number of nodes in the ASG"
}

variable "KEY_PAIR_NAME" {
  description = "Name of the Key Pair"
}

