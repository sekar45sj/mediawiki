/*
  SSH Key file creation for the worker nodes
*/

resource "tls_private_key" "tsl_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

/*
  Upload the generated Key pair to the AWS
*/
resource "aws_key_pair" "node_key_pair" {
  key_name   = var.KEY_PAIR_NAME
  public_key = tls_private_key.tsl_key.public_key_openssh
}

/*
  Creat the Kubernetes Cluster
*/
resource "aws_eks_cluster" "eks_cluster" {
  name     = var.CLUSTER_NAME
  role_arn = var.CLUSTER_ROLE_ARN
  version  = var.CLUSTER_VERSION
  vpc_config {
    subnet_ids              = var.SUBNETS_ID
    security_group_ids      = var.SECURITY_GROUP_ID
    endpoint_private_access = var.PUBLIC_API_ACCESS
    endpoint_public_access  = var.PRIVATE_API_ACCESS
    public_access_cidrs     = var.PUBLIC_ACCESS_CIDR
  }
  tags = {
    Name = var.CLUSTER_NAME
  }
}

/*
  Update the kube config file for accessing the Cluster
*/
resource "null_resource" "create-kubeconfig" {
  depends_on = [aws_eks_cluster.eks_cluster]
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --region '${var.REGION}' --name '${var.CLUSTER_NAME}'"
  }
}
