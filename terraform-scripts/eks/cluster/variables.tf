variable "CLUSTER_NAME" {
  description = "Name of the EKS Cluster"
}

variable "CLUSTER_ROLE_ARN" {
  description = "IAM role created for the Cluster"
}

variable "CLUSTER_VERSION" {
  description = "Version of the Kubernetes to be installed"
}

variable "SUBNETS_ID" {
  description = "Subnets in which the cluster have to be created. Minimum 2 subnets in different AZ"
}

variable "PUBLIC_API_ACCESS" {
  description = "Enable the public API access"
}

variable "PRIVATE_API_ACCESS" {
  description = "Enable the private API access"
}

variable "PUBLIC_ACCESS_CIDR" {
  description = "CIDR for the public Access"
}

variable "SECURITY_GROUP_ID" {
  description = "Security group for the Cluster to be attached"
}

variable "KEY_PAIR_NAME" {
  description = "Name of the Key Pair"
}

variable "REGION" {
  description = "AWS Region"
}
