output "endpoint" {
  value = aws_eks_cluster.eks_cluster.endpoint
}

output "cluster-name" {
  value = aws_eks_cluster.eks_cluster.id
}

