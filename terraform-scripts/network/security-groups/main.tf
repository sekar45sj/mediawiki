/*
    Creates dynamic security groups with the ports specified
*/
resource "aws_security_group" "security-group" {
  name        = var.SECURITY_GROUP_NAME
  vpc_id      = var.VPC_ID

  dynamic "ingress" {
    iterator = ingress_map
    for_each = var.INGRESS_MAP
    content {
      from_port   = ingress_map.value.port
      to_port     = ingress_map.value.port
      protocol    = ingress_map.value.protocol
      cidr_blocks = ingress_map.value.cidr_blocks
    }
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.EGRESS_CIDR_BLOCK
  }

  tags = {
    Name = var.SECURITY_GROUP_TAG_NAME
  }
}
