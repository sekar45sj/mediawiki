variable "SECURITY_GROUP_NAME" {
  default     = "my-security-group"
  description = "Name of the security groups"
}

variable "SECURITY_GROUP_TAG_NAME" {
  default     = "my-security-group"
  description = "Tag Name of the security groups"
}

variable "INGRESS_MAP" {
  default = [
    {
      port  = "22"
      protocol  = "tcp"
      cidr_clocks  = ["0.0.0.0/0"]
    }
  ]
  description = "Ports to be added in In-bound rule"
}

variable "EGRESS_CIDR_BLOCK" {
  default     = ["0.0.0.0/0"]
  description = "Out-Bound CIDR value for the RULE"
}

variable "VPC_ID" {
  description = "VPC ID where the routing have to be created"
}
