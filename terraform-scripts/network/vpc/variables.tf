variable "VPC_CIDR" {
  description = "CIDR block of the VPC"
}

variable "VPC_TAG_NAME" {
  description = "Tags to be added to the VPC"
}