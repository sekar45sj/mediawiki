/*
  Creates VPC in the mentioned CIDR
*/
resource "aws_vpc" "vpc" {
  cidr_block           = var.VPC_CIDR
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  tags = {
    Name = var.VPC_TAG_NAME
  }

}
