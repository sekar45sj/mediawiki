
variable "VPC_ID" {
  description = "VPC where the subnets have to be created"
}

variable "SUBNET_MAP" {
  description = "Details of the Subnets with in the VPC range"
}

variable "ASSIGN_PUBLIC_IP" {
  description = "Auto assign public IP for the instances created in those subnets"
}
