/*
  Create subnets with the CIDR provided in the specified VPC
*/

resource "aws_subnet" "subnet" {
  count                   = length(var.SUBNET_MAP)
  vpc_id                  = var.VPC_ID
  availability_zone       = var.SUBNET_MAP[count.index].availability_zone
  cidr_block              = var.SUBNET_MAP[count.index].cidr_block
  map_public_ip_on_launch = var.ASSIGN_PUBLIC_IP
  tags = {
    Name = var.SUBNET_MAP[count.index].tag_name
  }
}
