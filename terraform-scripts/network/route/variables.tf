variable "VPC_ID" {
  description = "VPC ID where the routing have to be created"
}

variable "PUBLIC_SUBNETS" {
  description = "List of Public subnets which have to be routed"
}

variable "IGW_TAG_NAME" {
  description = "Internet Gateway Tag Name"
}

variable "IGW_RT_TAG_NAME" {
  description = "Internet Gateway Route Table Tag Name"
}

variable "IGW_RT_CIDR_BLOCK" {
  description = "Internet Gateway Route Table CIDR blocks"
}
