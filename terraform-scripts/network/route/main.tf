/*
    This Terraform file creates Internet gateway and associate them with subnets specified
*/

/*
    Creates Internet-gateway
*/
resource "aws_internet_gateway" "igw" {
  vpc_id = var.VPC_ID
  tags = {
    Name = var.IGW_TAG_NAME
  }
}

/*
    Creates route table for Internet gateway
*/
resource "aws_route_table" "igw-route-table" {
  vpc_id = var.VPC_ID
  route {
    cidr_block = var.IGW_RT_CIDR_BLOCK
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = var.IGW_RT_TAG_NAME
  }
}

/*
    Associate public subnet with internet-gateway route table
*/
resource "aws_route_table_association" "public-subnet-association" {
  count          = length(var.PUBLIC_SUBNETS)
  subnet_id      = var.PUBLIC_SUBNETS[count.index]
  route_table_id = aws_route_table.igw-route-table.id
}
