variable "SUBNET_GROUP_NAME" {
  description = "Name of the RDS DB Subnet"
}

variable "DB_INSTANCE_NAME" {
  description = "Name of the RDS DB Instance"
}

variable "DB_NAME" {
  description = "Name of the RDS DB"
}

variable "SUBNETS_ID" {
  description = "Sunbets in which the RDS instance have to be created"
}

variable "ENGINE" {
  description = "Type of DB engine"
}

variable "ENGINE_VERSION" {
  description = "Version of the DB engine. Default for postgres engine"
}

variable "INSTANCE_CLASS" {
  description = "Instance type of the DB instance"
}

variable "ALLOCATED_STORAGE" {
  description = "Storage value of the DB instance"
}

variable "MAX_ALLOCATED_STORAGE" {
  description = "Threshold value of the DB instance. If it is give. Auto scaling is assigned automatically"
}

variable "STORAGE_TYPE" {
  description = "Storage type of the DB Instance"
}

variable "DB_USERNAME" {
  description = "User name for the DB created"
}

variable "DB_PASSWORD" {
  description = "Password for the DB created"
}

variable "PORT" {
  description = "Port of the DB instance"
}

variable "SECURITY_GROUP_ID" {
  description = "Security group to be attached to the RDS DB Created"
}

variable "MULTI_AZ" {
  description = "Need Stand By instance in another AZ. If enabled the cost is doubled"
}

variable "PUBLIC_ACCESS" {
  description = "Enable public access for the DB"
}

variable "SKIP_FINAL_SNAPSHOT" {
  description = "While deleting the DB do need to take final DB backup"
}

variable "VPC_ID" {
  description = "VPC ID for the security group"
}

variable "VPC_CIDR" {
  description = "VPC_CIDR for the RDS securoty group"
}

variable "RDS_SEURITY_GROUP_NAME" {
  description = "Name of the RDS Security Group"
}

