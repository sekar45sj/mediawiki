/*
  Create DB subnet group with the subnets given
*/
resource "aws_db_subnet_group" "db-subnet-group" {
  name       = var.SUBNET_GROUP_NAME
  subnet_ids = var.SUBNETS_ID
  tags = {
    Name = var.SUBNET_GROUP_NAME
  }
}

/*
    Security group for RDS port allow
*/
resource "aws_security_group" "rds-security-group" {
  name   = var.RDS_SEURITY_GROUP_NAME
  vpc_id = var.VPC_ID
  ingress {
    from_port       = var.PORT
    to_port         = var.PORT
    protocol        = "tcp"
    cidr_blocks     = [var.VPC_CIDR]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
  }
  tags = {
    Name = var.RDS_SEURITY_GROUP_NAME
  }
}

/*
  Create RDS instance with the parameters specified
*/
resource "aws_db_instance" "rds-database" {
  identifier             = var.DB_INSTANCE_NAME
  engine                 = var.ENGINE
  engine_version         = var.ENGINE_VERSION
  instance_class         = var.INSTANCE_CLASS
  allocated_storage      = var.ALLOCATED_STORAGE
  max_allocated_storage  = var.MAX_ALLOCATED_STORAGE
  storage_type           = var.STORAGE_TYPE
  name                   = var.DB_NAME
  username               = var.DB_USERNAME
  password               = var.DB_PASSWORD
  port                   = var.PORT
  vpc_security_group_ids = [ aws_security_group.rds-security-group.id ]
  db_subnet_group_name   = aws_db_subnet_group.db-subnet-group.id
  multi_az               = var.MULTI_AZ
  publicly_accessible    = var.PUBLIC_ACCESS
  skip_final_snapshot    = var.SKIP_FINAL_SNAPSHOT

}

