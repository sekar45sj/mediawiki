/*
  AWS Region
*/
variable "REGION" {
  default     = "ap-south-1"
  description = "AWS Region"
}



/*
  Variables for Network resoure like VPC, Subnet, Seurity Group & Route Table
*/
variable "VPC_CIDR" {
  default     = "10.1.0.0/16"
  description = "CIDR block of the VPC"
}

variable "VPC_TAG_NAME" {
  default     = "my-vpc"
  description = "Tags to be added to the VPC"
}

variable "SUBNET_MAP" {
  default     = [ 
    {
      "cidr_block": "10.1.1.0/24",
      "tag_name": "my-subnet-1",
      "availability_zone": "ap-south-1a"
    },
    {
      "cidr_block": "10.1.2.0/24",
      "tag_name": "my-subnet-2",
      "availability_zone": "ap-south-1b"
    },
    {
      "cidr_block": "10.1.3.0/24",
      "tag_name": "my-subnet-3",
      "availability_zone": "ap-south-1c"
    }
  ]
  description = "Details of the Subnets with in the VPC range"
}

variable "ASSIGN_PUBLIC_IP" {
  default     = true
  description = "Auto assign public IP for the instances created in those subnets"
}

variable "SECURITY_GROUP_NAME" {
  default     = "my-security-group"
  description = "Name of the security groups"
}

variable "SECURITY_GROUP_TAG_NAME" {
  default     = "my-security-group"
  description = "Tag Name of the security groups"
}

variable "INGRESS_MAP" {
  default     = [
    {
      "port" : "22"
      "protocol" : "tcp"
      "cidr_blocks" : [ "0.0.0.0/0" ]
    }
  ]
  description = "Ports to be added in In-bound rule"
}

variable "EGRESS_CIDR_BLOCK" {
  default     = [ "0.0.0.0/0" ]
  description = "Out-Bound CIDR value for the RULE"
}

variable "IGW_TAG_NAME" {
  default     = "my-internet-gateway"
  description = "Internet Gateway Tag Name"
}

variable "IGW_RT_TAG_NAME" {
  default     = "my-route-table"
  description = "Internet Gateway Route Table Tag Name"
}

variable "IGW_RT_CIDR_BLOCK" {
  default     = "0.0.0.0/0"
  description = "Internet Gateway Route Table CIDR blocks"
}



/*
  Variables for EKS Cluster & Node Group 
*/
variable "CLUSTER_NAME" {
  default     = "eks-cluster"
  description = "Name of the EKS Cluster"
}

variable "CLUSTER_VERSION" {
  default     = "1.16"
  description = "Version of the Kubernetes to be installed"
}

variable "PUBLIC_API_ACCESS" {
  default     = true
  description = "Enable the public API access"
}

variable "PRIVATE_API_ACCESS" {
  default     = true
  description = "Enable the private API access"
}

variable "PUBLIC_ACCESS_CIDR" {
  default     = [ "0.0.0.0/0" ]
  description = "CIDR for the public Access"
}

variable "NODE_GROUP_NAME" {
  default     = "my-node-group"
  description = "Node Group Name"
}

variable "ENVIRONMENT_NAME" {
  default     = "dev"
  description = "Environment Name"
}

variable "KEY_PAIR_NAME" {
  default     = "my-key-pair"
  description = "Name of the Key Pair"
}

variable "AMI_TYPE" {
  default     = "AL2_x86_64"
  description = "AMI type of the Node Group"
}

variable "INSTANCE_TYPE" {
  default     = "t3a.micro"
  description = "Instance type of the Node Group"
}

variable "DISK_SIZE" {
  default     = 30
  description = "Disk Size of the instance in the Node Group."
}

variable "DESIRED_NODES" {
  default     = 3
  description = "Desired number of nodes to run"
}

variable "MAX_NODES" {
  default     = 5
  description = "Maximum number of nodes in the ASG"
}

variable "MIN_NODES" {
  default     = 1
  description = "Minimum number of nodes in the ASG"
}



/*
  Variables for RDS
*/
variable "SUBNET_GROUP_NAME" {
  default     = "my-subnet-group"
  description = "Name of the RDS Subnet Group"
}

variable "DB_INSTANCE_NAME" {
  default     = "my-database"
  description = "Name of the RDS DB Instance"
}

variable "DB_NAME" {
  default     = "media_wiki"
  description = "Name of the RDS DB"
}

variable "ENGINE" {
  default     = "mariadb"
  description = "Type of DB engine"
}

variable "ENGINE_VERSION" {
  default     = "10.4.8"
  description = "Version of the DB engine. Default for postgres engine"
}

variable "INSTANCE_CLASS" {
  default     = "db.t2.micro"
  description = "Instance type of the DB instance"
}

variable "ALLOCATED_STORAGE" {
  default     = 20
  description = "Storage value of the DB instance"
}

variable "MAX_ALLOCATED_STORAGE" {
  default     = 100
  description = "Threshold value of the DB instance. If it is give. Auto scaling is assigned automatically"
}

variable "STORAGE_TYPE" {
  default     = "gp2"
  description = "Storage type of the DB Instance"
}

variable "DB_USERNAME" {
  default     = "admin"
  description = "User name for the DB created"
}

variable "DB_PASSWORD" {
  default     = "AdminPass"
  description = "Password for the DB created"
}

variable "PORT" {
  default     = 3306
  description = "Port of the DB instance"
}

variable "MULTI_AZ" {
  default     = false
  description = "Need Stand By instance in another AZ. If enabled the cost is doubled"
}

variable "PUBLIC_ACCESS" {
  default     = false
  description = "Enable public access for the DB"
}

variable "SKIP_FINAL_SNAPSHOT" {
  default     = true
  description = "While deleting the DB do need to take final DB backup"
}

variable "RDS_SEURITY_GROUP_NAME" {
  default     = "my-rds-security-group"
  description = "Name of the RDS Security Group"
}

