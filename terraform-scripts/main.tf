/*
    It is the main file which calls the modules and create the services
*/
provider "aws" {
  region = var.REGION
  ignore_tags {
    key_prefixes = ["kubernetes.io/"]
  }
}

/*
    Uses the VPC module mentioned in the source path
*/
module "vpc" {
  source       = "./network/vpc"
  VPC_CIDR     = var.VPC_CIDR
  VPC_TAG_NAME = var.VPC_TAG_NAME
}

/*
    Uses the Subnets module mentioned in the source path
*/
module "subnets" {
  source             = "./network/subnets"
  VPC_ID             = module.vpc.vpc_id
  ASSIGN_PUBLIC_IP   = var.ASSIGN_PUBLIC_IP
  SUBNET_MAP         = var.SUBNET_MAP
}

/*
    Uses the Route module mentioned in the source path
*/
module "routes" {
  source            = "./network/route"
  VPC_ID            = module.vpc.vpc_id
  PUBLIC_SUBNETS    = module.subnets.subnets
  IGW_TAG_NAME      = var.IGW_TAG_NAME
  IGW_RT_TAG_NAME   = var.IGW_RT_TAG_NAME
  IGW_RT_CIDR_BLOCK = var.IGW_RT_CIDR_BLOCK
}

/*
    Uses the Security groups module mentioned in the source path
*/
module "security-groups" {
  source              = "./network/security-groups"
  VPC_ID              = module.vpc.vpc_id
  SECURITY_GROUP_NAME = var.SECURITY_GROUP_NAME
  INGRESS_MAP         = var.INGRESS_MAP
}




/*
    Uses the RDS module mentioned in the source path to create RDS DB
*/
module "rds" {
  source                = "./rds"
  DB_NAME               = var.DB_NAME
  DB_INSTANCE_NAME      = var.DB_INSTANCE_NAME
  SUBNET_GROUP_NAME     = var.SUBNET_GROUP_NAME
  ENGINE                = var.ENGINE
  ENGINE_VERSION        = var.ENGINE_VERSION
  INSTANCE_CLASS        = var.INSTANCE_CLASS
  ALLOCATED_STORAGE     = var.ALLOCATED_STORAGE
  MAX_ALLOCATED_STORAGE = var.MAX_ALLOCATED_STORAGE
  STORAGE_TYPE          = var.STORAGE_TYPE
  DB_USERNAME           = var.DB_USERNAME
  DB_PASSWORD           = var.DB_PASSWORD
  PORT                  = var.PORT
  SUBNETS_ID            = module.subnets.subnets
  VPC_ID                = module.vpc.vpc_id
  VPC_CIDR              = var.VPC_CIDR
  SECURITY_GROUP_ID     = module.security-groups.security-groups
  MULTI_AZ              = var.MULTI_AZ
  PUBLIC_ACCESS         = var.PUBLIC_ACCESS
  SKIP_FINAL_SNAPSHOT   = var.SKIP_FINAL_SNAPSHOT
  RDS_SEURITY_GROUP_NAME= var.RDS_SEURITY_GROUP_NAME
}



/*
  Create the EKS Cluster Role
*/
module "eks-cluster-iam-role" {
  source             = "./eks/cluster-role"
  CLUSTER_ROLE_NAME  = format("%s-role", var.CLUSTER_NAME)
}

/*
  Create the EKS Node Group Role
*/
module "eks-nodes-iam-role" {
  source                = "./eks/node-groups-role"
  NODE_GROUP_ROLE_NAME  = format("%s-role", var.NODE_GROUP_NAME)
}

/*
  Create the EKS cluster
*/
module "eks-cluster" {
  source             = "./eks/cluster"
  CLUSTER_NAME       = var.CLUSTER_NAME
  CLUSTER_ROLE_ARN   = module.eks-cluster-iam-role.cluster-role-arn
  CLUSTER_VERSION    = var.CLUSTER_VERSION
  SUBNETS_ID         = module.subnets.subnets
  SECURITY_GROUP_ID  = [ module.security-groups.security-groups ]
  PUBLIC_API_ACCESS  = var.PUBLIC_API_ACCESS
  PRIVATE_API_ACCESS = var.PRIVATE_API_ACCESS
  PUBLIC_ACCESS_CIDR = var.PUBLIC_ACCESS_CIDR
  KEY_PAIR_NAME      = var.KEY_PAIR_NAME
  REGION             = var.REGION
}

/*
  Create the common node group
*/
module "eks-node-group" {
  source          = "./eks/node-groups"
  NODE_GROUP_NAME = var.NODE_GROUP_NAME
  NODE_ARN        = module.eks-nodes-iam-role.eks-node-arn
  CLUSTER_NAME    = module.eks-cluster.cluster-name
  SUBNETS_ID      = module.subnets.subnets
  MIN_NODES       = var.MIN_NODES
  MAX_NODES       = var.MAX_NODES
  DESIRED_NODES   = var.DESIRED_NODES
  AMI_TYPE        = var.AMI_TYPE
  DISK_SIZE       = var.DISK_SIZE
  INSTANCE_TYPE   = var.INSTANCE_TYPE
  REGION          = var.REGION
  KEY_PAIR_NAME   = var.KEY_PAIR_NAME
  LABELS = {
    Env  = var.ENVIRONMENT_NAME
  }
}

