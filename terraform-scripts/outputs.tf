output "vpc" {
  value = module.vpc.vpc_id
}

output "subnets" {
  value = module.subnets.subnets
}

output "security_group" {
  value = module.security-groups.security-groups
}

output "rds_hostname" {
  value = module.rds.hostname
}

output "eks_cluster_role_arn" {
  value = module.eks-cluster-iam-role.cluster-role-arn
}

output "eks_endpoint" {
  value = module.eks-cluster.endpoint
}

