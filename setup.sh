#!/bin/bash

# Install Terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform -y

# Install AWS CLI
sudo apt install awscli -y

# Install kubectl
sudo apt-get update && sudo apt-get install apt-transport-https gnupg2 curl -y
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update && sudo apt-get install kubectl -y

# Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
bash get_helm.sh

# Execute Terraform Scripts
cd terraform-scripts

# Initialize Terraform Working Directory
terraform init
if [ $? -ne 0 ]; then
  echo "Terraform Initialization Failed.."
fi

# Generating Terraform Plan
terraform plan
if [ $? -ne 0 ]; then
  echo "Terraform Plan Failed.."
fi

# Applying Terraform
terraform apply --auto-approve
if [ $? -ne 0 ]; then
  echo "Terraform Exeution Failed.."
fi



# Creating Namespace
namespace="dev"
kubectl create ns ${namespace}


# Applying Helm Chart for Mediawiki Deployment
resourceUrl="https://releases.wikimedia.org/mediawiki/1.34/mediawiki-1.34.3.tar.gz"
rds_hostname=$(terraform output rds_hostname)

cd ../helm-chart
sed -i "s/REPLACE_RDS_HOSTNAME/${rds_hostname}/g" application/tpl-values.yaml
cat application/tpl-values.yaml | sed "s|REPLACE_RESOURCE_URL|${resourceUrl}|g" > application/values.yaml

helm install application application/ -n ${namespace}


# Applying Helm Chart for Metric Server
helm install metrics-server metrics-server/ --set apiService.create=true -n kube-system

# Applying Helm Chart for cluster Autoscalar
helm install autoscaler autoscaler/ -n kube-system
