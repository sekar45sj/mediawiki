#!/bin/bash

# Getting Latest Resource URL and Deploy with same.
resourceUrl=$1
if [ -z "${resourceUrl}" ]; then
  echo "Resource URL is empty"
  exit 1
fi

namespace="dev"

cd helm-chart
cat application/tpl-values.yaml | sed "s|REPLACE_RESOURCE_URL|${resourceUrl}|g" > application/values.yaml

helm upgrade application application/ -n ${namespace}

