# Overview

Build a AWS Infrastructure using Terraform Scripts & Deploy Mediawiki Application in Kubernetes using Helm Chart.


# Architecture Diagram

[https://gitlab.com/sekar45sj/mediawiki/-/blob/develop/architecture.PNG](https://gitlab.com/sekar45sj/mediawiki/-/blob/develop/architecture.PNG)



# Setup Process


**1. Create EC2 Instance**  
Create EC2 Instance for executing script to build Infrastructure.  
AMI : Ubuntu 16.04 [setup.sh script written for ubuntu machine]  

**2. Attach IAM Role or Configure AWS using Access Key and Secret Key**  
Create a Role with Admin Access and Attah to EC2 Instance.  
Store AWS Access Key & Secret Key in EC2 instance using aws configure.  

**3. Clone Git Repository**  
Clone the Git Repository  

Command : 
`git clone https://gitlab.com/sekar45sj/mediawiki.git`

**4. Execute setup.sh script**  
Execute the setup.sh

Command :  
`cd mediawiki`  
`bash setup.sh`  

It perform below operations.  

**Installing below required software's.**  
- Terraform  
- AWS CLI  
- Helm  
- kubectl  


**Build Infrastructure by executing Terraform**  
While executing terraform, it will create below aws resources.  
- VPC  
- Subnet  
- Route Table  
- Security Group  
- EKS Cluster & Cluster Role  
- EKS Cluster Node Group & Node Group Role  
- S3 Bucket & Bucket Policy  
- RDS MariaDB Instance  


**Apply Helm Chart to Deploy Kubernetes Resources**  
- Applying Helm Chart for Metric Server  
- Applying Helm Chart for Cluster AutoScalar  
- Applying Helm Chart for Mediawiki Deployment  


**5. Get LoadBalancer EndPoint**  
Once the above script executed, Mediawiki Application has been deployed in Kubernetes.  
Get the LoadBalancer EndPoint from AWS Console or Get it by using below command. 

Command :  
`kubectl get svc -n dev` [Get External-IP]  


**6.Update Kubernetes Deployment**  
Created script for updating deployment  
Script Name : update-deployment.sh  
Parameters to Pass : Resource URL (Mediawiki Tar URL)  
Command to Execute : `bash update-deployment <resource-url>`


**7. Uninstall Helm Charts**  
Get Helm Chart list using below command  
`helm ls --all-namespaces`  

Uninstall Hel Chart using below command  
`helm uninstall <release-name> -n <namespace>`

**8. Destroy Terraform**  
Destroy the infrastructure using below command  
`terraform destroy --auto-approve`



# Optional Setup  

Building docker image and used in Kubernetes Deployment. This has to be execute before starting setup process.  
For this process need to have any docker registry or can use public docker registry.  

Build a Dockefile using below command  
`cd docker-file`  
`docker build -t <repository>:<version> .`  
`docker login`  
`docker push <repository>:<version>`  
`docker logout`  

Once uploaded a docker image, change image name and version in values.yaml file.  
File path : helm-chart/application/values.yaml  


**Notes :**  
I have already build and pushed docker image in public docker registry & which was mentioned in values.yaml.  
If you don't want to build docker image, then leave as it is.


# Input Details

All the aws resource details has been hardcoded in terraform-scripts/variables.tf file.  
Kindly change the values as required.

If not changing any values, please make sure below conditions.  
- VPC should not exists with cidr range (10.1.0.0/16).  
- EKS Cluster should not exists with name eks-cluster.  
- RDS DB Instance should not exists with name my-database.  
- RDS DB Subnet Group should not exists with name my-subnet-Group.  
- IAM role should not exists with name eks-cluster-role & my-node-group-role.   
- Key Pair should not exists with name my-key-pair. 

Default values has been defined in helm-chart variables.yaml file.
If needed, change as required.



# Implementation Details:
- Creating VPC with 2 public Subnets.
- Created Internet Gateway and attached to VPC.
- Created Route Table and added route for Internet access and subnet associated.  
- Created Security Group for Instance.  
- Created EKS Cluster Role & Node Group Role.  
- Created EKS Cluster and Node Group.  
- Created RDS MariaDB Instance with database name "media_wiki"
 
- Created Deployment for Mediawiki.  
- Created Horizontal Pod Autoscalar for Mediawiki Deployment.  
- Created Load Balancer Service for Mediawiki Deployment.  
- Created Metric Server to monitor resource utilization.  

